const InfluxDB = require('./datasources/influxdb');
const InfluxSchema = require('./datasources/schema');

const QuizStatus = {
    OPENED: 'opened',
    CLOSED: 'closed',
}

exports.getCurrentQuiz = async () => {
    const rows = await InfluxDB.query(`
        select * from ${InfluxSchema.Quiz.measurement}
        order by time desc limit 1
    `);
    return (rows.length !== 0 && rows[0].status !== QuizStatus.CLOSED) ? rows[0] : undefined;
};

exports.open = async (description, transactionID) => {
    await InfluxDB.writePoints([{
        measurement: InfluxSchema.Quiz.measurement,
        fields: {
            status: QuizStatus.OPENED,
            transactionID,
        },
        tags: {
            description,
        },
    }]);
}

exports.close = async (description, transactionID) => {
    await InfluxDB.writePoints([{
        measurement: InfluxSchema.Quiz.measurement,
        fields: {
            status: QuizStatus.CLOSED,
            transactionID,
        },
        tags: {
            description,
        },
    }]);
}

exports.getTimeInterval = async (description) => {
    const open = await InfluxDB.query(`
        select * from ${InfluxSchema.Quiz.measurement}
        where description = '${description}' and status = '${QuizStatus.OPENED}'
    `);
    if (open.length === 0) {
        return undefined;
    }
    const close = await InfluxDB.query(`
        select * from ${InfluxSchema.Quiz.measurement}
        where description = '${description}' and status = '${QuizStatus.CLOSED}'
    `)
    if (close.length === 0) {
        return undefined;
    }
    return {
        startTime: open[0].time.getNanoTime(),
        endTime: close[0].time.getNanoTime(),
    }
}