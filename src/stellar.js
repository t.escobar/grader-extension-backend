const StellarSDK = require('stellar-sdk');
const StellarConfig = require('./stellar.config');

const StellarServer = new StellarSDK.Server(StellarConfig.horizonURL);

const GraderAccount = StellarSDK.Keypair.fromSecret(StellarConfig.secretKey);

const transactionOptions = (process.env.NODE_ENV === 'production') ?
    {fee: StellarSDK.BASE_FEE, networkPassphrase: StellarSDK.Networks.PUBLIC} :
    {fee: StellarSDK.BASE_FEE, networkPassphrase: StellarSDK.Networks.TESTNET}

const transactionOperation = StellarSDK.Operation.payment({
    destination: GraderAccount.publicKey(),
    asset: StellarSDK.Asset.native(),
    amount: '1',
});

exports.saveMemo = async (memo) => {
    const account = await StellarServer.loadAccount(GraderAccount.publicKey());

    const stellarMemo = memo === undefined ?
        StellarSDK.Memo.text("NULL") :
        StellarSDK.Memo.hash(memo);

    const transaction = new StellarSDK.TransactionBuilder(account, transactionOptions)
        .addOperation(transactionOperation)
        .addMemo(stellarMemo)
        .setTimeout(60).build();

    transaction.sign(GraderAccount);

    const submissionResult = await StellarServer.submitTransaction(transaction);

    return submissionResult.id;
}
