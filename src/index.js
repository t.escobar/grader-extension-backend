const Express = require('express');

const BodyParser = require('body-parser');
const CORS = require('cors');

// import all middlewares
const LogMiddleware = require('./middlewares/log');

// import controller routers
const AssociationController = require('./controllers/association');
const SubmissionController = require('./controllers/submission');
const QuizController = require('./controllers/quiz');

const LogRepository = require("./repositories/log");

const http = require('http')
const https = require('https')
const fs = require('fs')
const path = require('path')

const App = Express();

// cross origin
App.use(CORS());

// for json body
App.use(BodyParser.urlencoded({ extended: false }));
App.use(BodyParser.json())

// for logging
App.use(LogMiddleware.prepareContext);

App.use('/admin', Express.static('public'))

App.use('/association', AssociationController);
App.use('/submission', SubmissionController);
App.use('/quiz', QuizController);

App.get('/log', async (req, res) => {
    res.json(await LogRepository.read());
})

App.use(LogMiddleware.logSuccess);
App.use(LogMiddleware.logError);

const port = parseInt(process.env['PORT'], 10) || 3000;
http.createServer(App).listen(port, () => {
    console.log(`application is running on port ${port}`)
})

if (process.env.NODE_ENV === 'production') {
    const cred = {
        key: fs.readFileSync(path.join(__dirname, '../cred/privkey.pem'), 'utf8'),
        cert: fs.readFileSync(path.join(__dirname, '../cred/cert.pem'), 'utf8'),
        ca: fs.readFileSync(path.join(__dirname, '../cred/chain.pem'), 'utf8')
    }

    const port = parseInt(process.env['HTTPS_PORT'], 10) || 3443;
    https.createServer(cred, App).listen(port, () => {
        console.log(`https application is running on port ${port}`)
    })
}
