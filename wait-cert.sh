#!/bin/bash

if [ "$NODE_ENV" = "production" ]
then
    until ls cred | grep .pem; do
        echo 'wait for cert'
        sleep 1
    done
fi

node src
